﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement; //loads scene management

public class PlayerController : MonoBehaviour
{

    public float StartSpeed;
    private float Speed;
    public float JumpDistance;
    private bool onGround = true;
    public Text countText;
    public Text winText;

    Rigidbody rb;

    private int count;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        count = 0;
        SetCountText();
        winText.text = "";
    }

    // Update is called once per frame
    void FixedUpdate()
    { 
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        onGround = Physics.Raycast(transform.position, Vector3.down, .52f); // checks to see if the ball is on the ground

        Vector3 movement = new Vector3(moveHorizontal, 0.0F, moveVertical);

        if (onGround == true) //checks to see if the onGround is true
        {
            Speed = StartSpeed + count; //Increases speed of player by the count of pickups they have
            rb.AddForce(movement * Speed);
        }
        else
        {
            rb.AddForce(movement * (Speed / 4)); //makes the player have less manueverability while in the air
        }

        if (transform.position.y <= -20)
        {
            RestartLevel();
        }
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
        else if (other.gameObject.CompareTag("JumpBlock")) //checks to see if the player passes through a jump block
        {
            Jump();
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 8)
        {
            winText.text = "You Win";
        }
    }

    void Jump() //Function for making the ball jump
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical"); // manages the movement of the ball on the x and z axis

        Vector3 movement = new Vector3(moveHorizontal, JumpDistance, moveVertical); //adds the force to make the ball jump

        rb.AddForce(movement * Speed);
    }

    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); //reloads the scene
    }

}
